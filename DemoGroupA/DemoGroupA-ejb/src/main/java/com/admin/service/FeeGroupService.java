/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.service;

import com.admin.dto.CollegeDto;
import com.admin.dto.FeeGroupDto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cc
 */
@Local
public interface FeeGroupService {
    
    public boolean save(FeeGroupDto feeGroupDto);

    boolean delete(FeeGroupDto feeGroupDto);
    
    boolean checkIfFeeGroupNameAlreadyExists(FeeGroupDto feeGroupDto);

    boolean checkIfFeeGroupCodeAlreadyExists(FeeGroupDto feeGroupDto);
    
    boolean update(FeeGroupDto feeGroupDto);
    
    List<FeeGroupDto> findByCollegeId(CollegeDto collegeDto);
    
    List<FeeGroupDto> findByCollegeIdForDropDown(CollegeDto collegeDto);
    
}
