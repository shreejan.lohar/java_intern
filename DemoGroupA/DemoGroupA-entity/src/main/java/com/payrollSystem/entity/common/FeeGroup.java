/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payrollSystem.entity.common;

import com.payrollSystem.entity.abstracts.AbstractCode;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author cc
 */
@Getter
@Setter
@Entity
@Table(name = "FEE_GROUP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FeeGroup.findAll", query = "SELECT f FROM FeeGroup f"),
    @NamedQuery(name = "FeeGroup.findById", query = "SELECT f FROM FeeGroup f WHERE f.id = :id")
 })
    
public class FeeGroup extends AbstractCode {

    @Column(name ="ISACTIVE", nullable = false)
    private boolean isActive=true;
    
}
